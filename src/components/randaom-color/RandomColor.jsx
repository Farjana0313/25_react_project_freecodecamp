import { React, useState } from 'react';

export default function RandomColor() {
    const [typeOfColor, setTypeOfColor] = useState('hex');
    const [color, setColor] = useState('#000000');

    return (
        <div style={{
            width: "100vw",
            height: "100vh",
            background: color,
            justifyContent: "center",
            alignItems: "top"
        }}>
            <button>Create HEX Color</button>
            <button>Create RGB Color</button>
            <button>Generate Random Color</button>
            <div
                style={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    color: "#fff",
                    fontSize: "60px",
                    marginTop: "50px",
                    flexDirection: 'column',
                    gap: '20px'
                }}
            >
                <h3>{typeOfColor === "rgb" ? "RGB Color" : "HEX Color"}</h3>
                <h1>{color}</h1>
            </div>
        </div>
    )
}
